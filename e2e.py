# Generated by Selenium IDE
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver import ChromeOptions

class Test_checkout():
  def setup_method(self, method):
    #configure chrome in headless to work in containers
    options = ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--disable-extensions')
    options.add_argument("--no-sandbox")
    self.driver = webdriver.Remote('http://localhost:4444/wd/hub', options.to_capabilities())

    self.driver.get("http://localhost:8000/")
    self.driver.set_window_size(1116, 864)
    #vérification présence onglet shop
    elements = self.driver.find_elements(By.CSS_SELECTOR, ".site-menu > li:nth-child(2) > a")
    assert len(elements) > 0
    #ouvrir onglet shop
    self.driver.find_element(By.CSS_SELECTOR, ".site-menu > li:nth-child(2) > a").click() 
    #vérification présence de l'image du tee-shirt
    elements = self.driver.find_elements(By.CSS_SELECTOR, ".block-4-image .img-fluid")
    assert len(elements) > 0
    #clic sur l'image du tee shirt
    self.driver.find_element(By.CSS_SELECTOR, ".block-4-image .img-fluid").click()
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_is_item_colour_green(self):
    #clic sur le champ quantity et mise à 1 du champ
    self.driver.find_element(By.ID, "id_quantity").click()
    self.driver.find_element(By.ID, "id_quantity").send_keys("1")
    #clic sur le champ couleur et mise à vert du champ
    self.driver.find_element(By.ID, "id_colour").click()
    dropdown = self.driver.find_element(By.ID, "id_colour")
    dropdown.find_element(By.XPATH, "//option[. = 'green']").click()
    self.driver.find_element(By.ID, "id_colour").click()
    #vérification de la présence de add to cart
    elements = self.driver.find_elements(By.CSS_SELECTOR, ".btn")
    assert len(elements) > 0
  
  def test_is_item_size_s(self):
    #vérification présence add to cart
    elements = self.driver.find_elements(By.CSS_SELECTOR, ".btn")
    assert len(elements) > 0
    #clic sur le champ taille et mise à la taille s du champ
    self.driver.find_element(By.ID, "id_size").click()
    dropdown = self.driver.find_element(By.ID, "id_size")
    dropdown.find_element(By.XPATH, "//option[. = 's']").click()
    self.driver.find_element(By.ID, "id_size").click()
    #clic sur add to cart
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    #on vérifie que le produit a bien été ajouté
    #présence de quantity
    self.driver.find_element(By.CSS_SELECTOR, ".form-control").click()
    element = self.driver.find_element(By.CSS_SELECTOR, ".form-control")
    assert element.is_enabled() is True
  
 
